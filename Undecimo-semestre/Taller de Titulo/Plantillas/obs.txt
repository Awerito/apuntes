- La introducción fue muy debil, poco atrayente y poco clara.
- No se identifica la importancia de la problemática
- Y por lo mismo, no se entiende por qué se debe hacer esta propuesta de solución

- No se entiende el paso desde la introducción al marco teórico.
- Tampoco se entiende el cambio al concepto de Visión por Computadora.
- Por qué llego a la gráfica de función Sigmoid, entonces falta una Hoja más general a modelos que explique las partes de cualquier modelo de ML. Luego las siguientes hojas tienen más sentido.

- De hecho, falta un apartado que defina los conceptos de modelo, entrenamiento y ajuste,
  métricas de evalaución y testeo de resultados. (No que el marco va evolucionando con el
  proyecto?)

- Para cada modelo, ejemplo general. (Tengo entendido que las imagenes son ejemplos)
