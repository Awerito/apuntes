Excuse me!                                 " ¡Con permiso!
Can you help me, please?                   " Puedes ayudarme, ¿Por favor?
How do you  say ... In English?            " ¿Como se dice ... En ingles?
            Spell ...?                     " dice ...?
            Pronounce ...?                 " Pronuncia ...?
Sorry?/Pardon?                             " Disculpa?/Perdón?
Could you write that on the board, please? " Tu podrías escribir eso en la pizarra, ¿Por favor?
What's the  opposite of ...?               " ¿Qué es lo opuesto de ...?
            Difference?                    " ¿Cual es la diferencia?
            Past tense of ...?             " ¿Cual es el tiempo pasado de ...?
What does ... Mean?                        " ¿Qué el lo que significa ...?
I'm sorry, I don't  understand.            " Lo siento, no lo entiendo.
                    Know.                  " Sé.
                    Remember.              " Recuerdo.
Is this right or wrong?                    " ¿Esto esta bien o mal?
Which page?                                " ¿En qué página?
Have you got a ... Please?                 " ¿Haz tenido un ... Por favor?
Here you are.                              " Aquí estas.
Can I go to the toilet, please?            " Puedo ir al baño, ¿Por favor?
Sorry I'm late.                            " Perdón, llegué tarde.
See you in (Monday)!                       " Te veo el (Lunes)!
Have a nice weekend!                       " Ten un buen fin de semana!
Can I borrow ... Please?                   " Puedo tomar ... ¿Por favor?
Can you pass ... Please?                   " Puedes pasarme ... ¿Por favor?
That's/That isn't mine.                    " Eso /no es mio.
Just a moment, please.                     " Solo un momento, ¿Por favor?
We haven't finished (yet)                  " No hemos terminado (todavía)
Come on. Hurry up!                         " Vamos. ¡Apresúrate!
Whose turn is it?                          " ¿A quien le toca?
It's my/your turn.                         " Es mi/tú turno.
You go first.                              " Tú vas primero.
What did he/she say?                       " ¿Qué fue lo que él/ella dijo?
It doesn't matter.                         " Ya no importa.
What/How about you?                        " ¿Qué hay de ti?/¿Qué pasa contigo?
I'm not sure.                              " No estoy seguro.
Perhaps./Maybe.                            " Quizás./Tal vez.
Let's ask the teacher.                     " Vamos a preguntarle al profesor.
