# Temas
- Grafos Bipartitos:
	1. Transporte
	2. Transbordo
	3. Asignación
- Grafo dirigido y no dirigido:
	1. Flujo máximo
	2. Camino más corto
	3. Árbol generador

### Problema 17:
|  | Investigación Restante | Desarrollo | Diseño de Manufactura | Inicio de Producción y Distrubución |
|--|--|--|--|--|
| Normal | 5 |  |  |  |
| Prioridad | 4 | 3 | 5 | 2 |
| Quiebre | 2 | 2 | 3 | 1 |
|  |  |  |  |  |
| Normal | 3 |  |  |  |
| Prioridad | 6 | 6 | 9 | 3 |
| Quiebre | 9 | 9 | 12 | 6 |

```mermaid
graph LR
a((Inv))-->b((Dev))
a-->b
a-->b
b-->c((Dis))
b-->c
c-->d((Prod))
c-->d
```
$$X_{ijt}=1,0$$
$$Min\ TP=5x_{NI}+4x_{PI}+2x_{QI}+...$$
$$3x_{NI}+6x_{PI}+9x_{QI}+...\le 30$$
$$\sum{x}-1=0$$
