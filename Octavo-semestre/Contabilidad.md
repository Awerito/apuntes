# Presupuestos
```mermaid
graph LR
a[Presupuestos]---b[Operacionales]
a---c[Financieros]
```

### Presupuesto de venta
Proyecciones de recursos en terminos de *Unidades*, *Precio* e *Ingresos* repartidos en *Plazos*.
> Exemplo
> 
|  | P1 | P2 | P3 | Total |
|--|--|--|--|--|
| Unidades | 1000 | 1500 | 1200 | 3700 |
| Precio | 2000 | 2000 | 2000 | 6000 |
| Ingreso Total | 2000000 | 3000000 | 2400000 | 7400000 |

#### Condiciones de Venta:
- 50% al contado
- Saldo 30 y 60 dias
### Producción
|  | P1 | P2 | P3 |  |
|--|--|--|--|--|
| Unidades Venta | 1000 | 1500 | 1200 | +
| Unidades Inv. Inicial | 0 | 100 | 150 | -
| Unidades Inv. Final | 100 | 150 | 120 | +
| Necesito Producir | 1100 | 1550 | 1170 | =

#### Política Inventario
Producto terminado 10% de la venta del mes
- Materiales: 
	- Cubierta $1\times1[m²]$ => $1\ Unidad\ de\ Conversion$ => $\$800$
	- Apoyos $2\times0.5[m²]$ => $1\  Unidad\ de\ Conversion$ => $\$400$
	- Inventario Final $15\%$ de ventas
- Mano de Obra:
	- $3Hrs \times silla$ => $\$2500$
- CIF:
	- Fijos $\$250000$
	- Variables $40\% MOD$

## Presupuesto para las Cubiertas
| Materia Prima | P1 | P2 | P3 |   |
|--|--|--|--|-- |
| Uni. Necesito Producir | 1100 | 1550 | 1170 | +
| Tasa de Conversion | 1 | 1 | 1 | x
| Necesito Material x Venta | 1100 | 1550 | 1170 | = +
| Uni. Mat. Inv. Inicial | 0 | 165 | 232 | -
| Uni. Mat. Inv. Final | 165 | 232 | 175 | +
| Nec. de Compra | 1265 | 1617 | 1113 | = +
| Precio de Compra | 800 | 800 | 800 | x
| Valor Compra | 1012000 | 1293600 | 890400 | =

## Presupuesto para las Patas(Apoyo)
> Tarea para la casa
## Presupuesto Mano de Obra
| Presupuesto MOD | P1 | P2 | P3 |  |
|--|--|--|--|--|
| Unid. Prod. | 1100 | 1550 | 1170 | +
| Tasa Conv. | 3 | 3 | 3 | x
| Total Horas | 3300 | 4650 | 3510 | = +
| N° Horas | 833,3 | 833,3 | 833,3 | x
| Total MOD | 2750000 | 3875000 | 2925000 | =

## Presupuesto CIF
|  | P1 | P2 | P3 |  |
|--|--|--|--|--|
| Total MOD | 2750000 | 3875000 | 2925000 | +
| Tasa CIF | $40\%$ | $40\%$ | $40\%$ | x
| CIF Var. | 1100000 | 1550000 | 1170000 | = +
| CIF Fijo | 250000 | 250000 | 250000 | +
| Total CIF | 1350000 | 180000 | 1420000 | =

> Presupuesto Gastos Admin
> Presupuesto Gastos Comercial
> Estado Resultado Proyectados
> Balances Proyectados
# Presupuestos Financieros
Flujo de caja (solo en efectivo)
| Ingresos | P1 | P2 | P3 | P4 | P5 |  |
|--|--|--|--|--|--|--|
| Ventas al Contado | 1000000 | 1500000 | 1200000 |  |  | +
| Ventas a 30 Dias |  | 500000 | 750000 | 600000 |  | +
| Ventas a 60 Dias |  |  | 500000 | 750000 | 600000 | +
| Ventas Activos Fijos | 0 |  |  |  |  | +
| Otras Ventas | 0 |  |  |  |  | +
| Total Ingreso | 1000000 | 2000000 | 2450000 | 1350000 | 600000 | =
| Egresos | --- | --- | --- | --- | --- | ---
| Mat. Prima 30 Dias|  | 337333 | 431200 |  |  |
| " 60 Dias |  |  | 337333 | 431200 |  |
| " 90 Dias |  |  |  | 337333 | 431200 |
| MOD | 2750000 | 3875000 | 2925000 |  |  |
| $-20\%$ CIF | 1350000 - 270000 | 1800000 - 360000 | 1420000 - 284000 |  |  |
| Gastos Admin | 0 | 0 | 0 | 0 | 0 |
| Gastos Comerciales | 0 | 0 | 0 | 0 | 0 |
| Otros Gastos | 0 | 0 | 0 | 0 | 0 |
| Total Egresos | 3830000 | 5652333 | 4829533 |  |  |

---
|  | P1 | P2 | P3 |  |
|--|--|--|--|--|--|
| Saldo Caja | 3000000 | 170000 | -3482333 | + |
| Total Ingres | 1000000 | 2000000 |  | + |
| Total Egresos | 3830000 | 5652333 |  | - |
| Saldo Final | 170000 | -3482333 |  | = |

### Costo Volumen Utilidad o Punto de equilibrio

El precio de venta esta dado por los siguientes factores:
- Costos:
    - Materia Prima
    - Mano de Obra
    - CIF
- Gastos:
    - Arriendo
    - Intereses
    - Seguridad
    - Otros
- Utilidad en porcentaje

Precio de venta:
$P_{venta}=C_{Variables}+C_{Fijos}$

Punto de Equilibrio:
$P_{eu}=\frac{Gastos\ Fijos-Depreciación}{(P_{v}-Cv)}$

Unidades Necesarias:
$U=\frac{C_{f}+Utilidad}{P_{v}-C_{v}}$
