```mermaid
graph LR
a[E]-->b[C]
b-->c[G]
c-->d[S]
a-->e[H]
e-->d
```
$$T(s)=\frac{C(s)G(s)}{1+C(s)G(s)H(s)}$$

## Método Routh y Hurwith
$F(s)=s⁵+s⁴+10s³+72s²+172s+240$
| $s^n$ | $a_0$ | $a_2$ | $a_4$ | $a_6$ |
|--|--|--|--|--|
| $s^{n-1}$ | $a_1$ | $a_3$ | $a_5$ | $a_7$ |
| $s^{n-2}$ | $b_1$ | $b_2$ | $b_3$ | $b_4$ |
| $s^{n-3}$ | $c_1$ | $c_2$ | $c_3$ | $c_6$ |

$$b_1=\frac{a_1a_2-a_0a_3}{a_1}$$
$$b_2=\frac{a_1a_4-a_0a_5}{a_1}$$
$$b_3=\frac{a_1a_6-a_0a_7}{a_1}$$
En el exemplo $F(s)=s⁵+s⁴+10s³+72s²+172s+240$:
$$a_0=1, a_1=1,a_2=10,a_3=72,a_4=172,a_5=240$$
| $s^5$ | $1$ | $10$ | $172$ | $0$ |
|--|--|--|--|--|
| $s^{4}$ | 1 | 72 | 240 | 0 |
| $s^{3}$ | -62 | -68 | 0 | 0 |
| $s^{2}$ | 70,9 | 240 | 0 | 0 |
| $s^{1}$ | 141,87 | 0 | 0 | 0 |
| $s^{0}$ | 240 | 0 | 0 | 0 |
2 Cambios de signo => 2 Raíces en el lado derecho (Inestrable)
> Un ceros se calculan asumiendo un $\epsilon$ muy pequeño
> Dos ceros se crea polinomio auxiliar y se deriva para nuevos coeficientes

# PID

## Control de lazo abierto

Control en donde no tengo retroalimentación de la salida del sistema.
Ejemplo: microondas, horno electrico, tostados.

Suelen ser componentes mas baratos.

## Control de lazo cerrado

Control dende se tiene retroalimentación de la salida para tener ajustes en el comportamiento del sistema. Suelen ser componentes mas caros.

#### Control Proporcional

$m=K_pe$, donde $m$ es la salida y $e$ la entrada.
No elimina errores o perturbaciones.

#### Control Integral

En un control de acción integral el valor de salida del controlador $m$ es proporcional a la integral de la señal de error.

$\frac{dm(t)}{dt}=K_ie(t)\ ó\ M(s)=\frac{K_i}{s}E(s)$

#### Control Proporcional-Integral

$M(s)=(K_p+\frac{K_i}{s})E(s)$

#### Control Proporcional-Derivativo

$M(s)=K_p(1 + T_ds)E(s)$

#### Control PID

$M(s)=K_p(1 + T_ds + \frac{1}{T_s}s)E(s)$

> Pagina de apuntes 298, creo

> - Definiciones PID
> - Para que sirve
> - Diferencias
> - Cuando se ocupan
