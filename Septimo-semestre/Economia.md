# Economía:
Ciencia social que usa método científico sin pertenecer al grupo de las ciencias exactas.

Estudia el comportamiento de los **agentes económicos** y la forma en que la sociedad se organiza para resolver el **problema económico**.

## Algunos agentes económicos son:
- Consumidores
- Empresas
- Gobierno
- Sector externo

## Recursos o factores productivos:
1. Recursos naturales (Tierra):
	- Bosques
	- Mares
	- Lagos y ríos
	- Yacimientos minerales
2. Recursos humanos (Trabajo):
	- Cantidad
	- Calidad
3. Capital físico (no financiero):
	- Maquinas
	- Equipos
	- Infraestructura para producir
	- Herramientas
4. Capacidad empresarial

## El problema económico:
El problema económico puede resumirse en suplir las necesidades de un determinado grupo social haciendo uso de los recursos disponibles.

En este problema se tienen tres situaciones:
 1. Recursos $>$ Necesidades:
	- Las necesidades dejan de ser un problema.
	- El grupo emigra a nuevas necesidades.
 2. Recursos $=$ Necesidades:
	- Las necesidades son cubiertas satisfactoriamente.
	- El grupo se estanca en su crecimiento.
 3. Recursos $<$ Necesidades:
	- Las necesidades no son cubiertas para la mayoría del grupo social.
	- El grupo disminuye su calidad de vida.

 ### Características de las  necesidades:
- Se pueden expresar por medio de la carencia de bienes y/o servicios necesarios dentro de la población.
- Pueden ser múltiples y hay quienes los definen como ilimitadas
- Son jerarquizables, es decir, que se van relegando a los estratos más bajos de una sociedad.
 ### Características de los recursos:
- Son bienes o servicios escasos o de difícil acceso para toda o una parte de la población.
- Tienen usos alternativos no necesariamente destinados en resolver alguna necesidad.

## Modelo de frontera de posibilidad de producción *(FPP)*
El modelo **FPP** es la gráfica de un bien $x$ vs otro $y$, la curva mostrada es lo que se denomina la **frontera** y es el nivel mas eficiente posible de producción de $y$ dado el nivel de producción de $x$.
Los puntos encontrados fuera de la *frontera* son puntos inalcanzables de producción para cualquiera de los bienes, llegado a estos puntos nos encontramos con una restricción de recursos.
Por último, los puntos debajo de esta frontera son resultados ineficientes de producción de los bienes que desperdician o desaprovechan recursos.
### Ejemplo:
![](https://www.enciclopediafinanciera.com/images/frontera-de-posibilidades-de-produccion1.png)
A continuación definimos el ***coste de oportunidad*** talque:

$$C_{o}= {-\ }\frac{\Delta y}{\Delta x}$$
Del cual podemos entender como el coste total en unidades que tendría la producción del bien $y$ para aumentar la producción del bien $x$.
## Conceptos relacionados a la _FPP_
 1. **Escasez:** Puntos externos a la frontera.
 2. **Elección:** Puntos pertenecientes a la frontera.
 3. **Insuficiencia:** Puntos bajo la frontera.
---
## Propiedades de la *FPP*
 - Las mejoras o aumentos de las tecnologías para las industrias de ambos bienes expande la Frontera para $x$ e $y$ así como también lo puede hacer el crecimiento económico.
![](https://www.aulafacil.com/Microeconomia/Imagens/Lecc2-4.gif)

---
***Ejercicio:*** Una economía produce plátanos $p$ y tomates $t$. Su dotación de recursos es de 4320 horas de trabajo. La cantidad de trabajo necesaria para producir un kilogramo de tomate es constante e igual a 10 horas, siendo la cantidad máxima de plátanos que puede producirse igual a 144 kg.

 1. Represente gráficamente la *FPP* de esta economía.
 2. Obtenga la expresión matemática de la *FPP*.
 3. Determine el costo de oportunidad de los plátanos $p$ en términos de tomates $t$.
 4. Indique si las siguientes combinaciones de producción de plátanos y tomates de la forma $(p,t)$ pertenecen al conjunto de posibilidades de producción de esta economía:
	 - $c(90, 170)$
	 - $d(100, 132)$
	 - $e(75, 200)$
>1. Buscando los puntos de intersección con los ejes:
>- Por dato tenemos el punto $(0, 144)$ para la cantidad máxima de producción de platanos.
>- Luego calculamos la máxima producción de tomate por medio de:
>$$\frac{max(recursos)}{max(horas)}=\frac{4320hrs}{10\frac{hrs}{kg}} = 432 kg = m$$
>$$\Rightarrow(432, 0) \land(0,144)$$
>Finalmente al ser una relación constante queda una recta tal que:
>![enter image description here](https://lh3.googleusercontent.com/vR-n9EiQVVN_BTxHzjRSoIAAxfWTMM2tiJib_TQbp81T2RdtNmufe_9Q0Ufgi6sxKwL4bBcsyxQ)
---
> 2. Sabiendo que la recta es de la forma $t = m - C_{o}\times p$ con $m = 432$:
> $$t = 432 - \frac{\Delta t}{\Delta p}p$$
> $$t = 432 - \frac{432}{144}p$$
> Finalmente la recta queda tomates en función de los plátanos queda:
> $$t = 432-3p$$.
---
>3. Por medio de $C_{o}$ calculado anteriormente sabemos que:
>$$- \frac{\Delta t}{\Delta p}= - \frac{432}{144}=-3$$
>Por lo que sabemos que en cada oportunidad se deberán disminuir la producción $p$ unidades para favorecer a $t$
---
>4. Gráfico de los puntos dados:
>![enter image description here](https://lh3.googleusercontent.com/YntJSkyJjaumnre8dsqofXSAXClkf2uK-zqz2Y8H614kp-578Wm0gw8qEfCX1mdldNNKILu_2hE)
---
## Microeconomía
Estudia el comportamiento de los agentes económicos y de los marcados individualmente considerados.
## Macroeconomía
Estudia los grandes agregados económicos y el conjunto de los mercados (Salvo por el mercado del trabajo, que es un mercado especifico que se estudia puntualmente).
Con grandes agregados económicos no referimos a:
- Producción
- Empleo
- Inflación
- Intercambio comercial
### Variables
```mermaid
graph LR;
A(Variable)-->B(Dependiente);
A-->C(Independiente);
```
***Ejemplo:*** $y=f(x)$, donde decimos que $y$ depende directamente de los valores que tome $x$.
```mermaid
graph LR;
A(Variable)-->B(Exógenas);
A-->C(Endogenas);
```
Cuando hablamos de variables dentro de un modelo tenemos estos dos tipos de variables.
1. ***Exógenas:*** Su comportamiento no esta explicado por el modelo.
2. ***Endogenas:*** El modelo las considera su comportamiento.
 ```mermaid
graph LR;
A(Variable)-->B(Flujo);
A-->C(Stock);
```
En cuanto a la dimensionalidad tenemos lo que sigue.
1. ***Flujo:*** Tiene dimensión de tiempo.
2. ***Stock:*** Tiene dimensión de unidades.
### Economía Normativa
Es la rama del estudio de la economía que se centra en emitir juicios de valor, buscando explicar lo que debe ser.
### Economía Positiva
Trata lo que es, es decir, lo que se afirma son hechos contrastables con la realidad.
## Teoría de la Oferta y la Demanda
- **Mercado:** Es el lugar *físico o virtual* donde se transan los bienes y/o servicios.

Sea $q$ la cantidad demandada y $p$ su precio, definimos entonces:
$$q=f(p)/ q\propto p^{-1}$$
>- Los precios son señales para los agentes económicos.
>- Los precios son signo de abundancia o escasez de los bienes y servicios.
>- Los económicos normalmente grafican la variable dependiente en el eje de las abscisas (su autismo es preocupante pero es mejor no decirles nada, quizá que le hechan a tu café después :v)

# Función de demanda
Definamos la demanda como:
$$D = f(v_{1}, v_{2}, v_{3}, ...\ , v_{n})$$
Donde todos los $v_{i}$ son factores. Ejemplos de estos factores pueden ser:
- Precio $p$
- Ingresos del consumidor $y$
- Gustos del consumidor $g$
- Bienes Relacionados
	- Sustitutos: bienes que satisfacen las mismas necesidades. $p_{s}$
	- Complementarios: Son bienes que se consumen al mismo tiempo o seguidos uno de otro. $p_{c}$
- Otros factores $o_{f}$
$$D=f(p, y, g, p_{s}, p_{c}, o_{f})$$

### Relación precio y demanda
Recordemos que $q\propto p^{-1}$
> **Condición de análisis:** Solo se moverá o alterará una variable a la vez, las demás se mantendrán constantes.
> **Nota importante:** cuando se mueven las variables $y,g,p_{s},p_{c},o_{f}$ la curva de demanda sufrirá un desplazamiento horizontal. Aumento si el desplazamiento es hacia la derecha y Decremento si el desplazamiento es hacia la izquierda.
#### Relación gustos y demandas
1. Gustos a favor del bien: Aumenta la demanda.
2. Gustos en contra del bien: Decrece la demanda.

# Estructuras de mercado
### Competencia perfecta:
1. Gran cantidad de compradores y vendedores: Esto significa que ninguno de manera individual puede fijar el precio.
2. Los consumidores cuentan con la información completa.
3. Inexistencia de barreras de entrada y de salida al mercado.
### Monopolio
El monopolio implica la existencia de un solo productor y muchos compradores.
### Oligopólico
Significa pocas empresas en una industria, el principal riesgo en estos casos es la facilidad de estos para coludirse.
### Monopsonio
Cuando existe un único comprador.
### Duopolio
Cuando la competencia solo se reparte en dos productores.
## Relación demanda por variables
### Relación demanda $D$ vs ingresos $y$
$$D\propto y$$
> Esto quiere decir que dado que aumenta uno aumenta el otro y viceversa. A los bienes que siguen este comportamiento se les llama bienes normales, en el caso de no seguir esta regla se les llama bienes inferiores.
*Por ejemplo:* Ropa de segunda mano, cuando los ingresos aumentan, solemos dejar de lado el consumo de estas prendas.
### Relación demanda vs precio de un sustituto $p_{s}$
$$p_{x}\propto p_{s}$$
> Esto quiere decir que siempre que aumenta el precio de un sustituto aumentará también el precio del bien
### Relación demanda vs otros factores
- Población: Aumenta la demanda.
- Expectativas favorables: Aumentan la demanda.
- Expectativas desfavorables: Disminuyen la demanda.
#### Diferencia entre cambio en la cantidad demandada vs cambio en la demanda
Si se mueve el precio del bien se dice entonces que hay un cambio en la cantidad demandada, por otro lado, el cambio de la demanda produce un cambio en la curva completa y se produce cuando se ve alterada cualquier otra variable que no sea el precio.
#### Demanda de mercado
$$D_{Mercado}=D_{1}+D_{2}+...+D_{n}$$
$$D_{M}=\sum_{i=1}^{n} D_{i}$$
---
***Ejercicio:*** En relación con la demanda de los notebook, deduzca lo que sucedería en los siguientes casos:
1. Aumenta el precio de las tablets: *La demanda indicaría que al aumentar el precio de las tablet (producto sustituto) aumenta la demanda de los notebook.*
2. Una campaña publicitaria exitosa en favor de las tablets: *La demanda de los notebooks disminuiría.*
3. Se aplica un impuesto a algunos componentes de los notebooks: *Esto causaría un aumento en el los precios, lo que se traduce en una disminución en la **cantidad demandada.***
4. Aumenta el precio de los televisores: *Asumiendo que son complementarios entonces la demanda disminuye.*
5. Aumenta el precio de la vara de leña: *Son bienes independientes, por lo que no se alteraría la demanda de los notebook.*

# Teoría de la oferta
$$S = f(Precio, Costo, Tecnología, Otros)$$
$$S = f(p, c_{f}, t, o_{f})$$
#### Relación oferta vs precio
$$q_{s} \propto p$$
> Si los precios suben, creo mas producto y vice versa.
#### Relación oferta vs costo de factores producidos
$$q_{s}\propto c_{f}^{-1}$$
> Al Aumentar el costo de los factores productivos la oferta disminuye.
#### Relación oferta vs tecnología
Al aumentar la tecnología aumenta la oferta, es decir, $q_{s} \propto p$.
Es raro ver en el mundo real se disminuya la tecnología.
#### Relación oferta vs otro factores
$$q_{s} \propto t$$
> Si las expectativas sobre la economía son positivas lo podríamos traducir en un aumento de oferta.
## Equilibrio de mercado
1. El equilibrio del mercado es la intersección de la curva de demanda $D$ con la curva de oferta $S$.
Recordando que la pendiente de la demanda es negativa y la oferta positiva.
2. En el punto de intersección entre estas dos curvas tenemos que la demandada y la oferta son iguales, es decir, existe un único precio $p_{e}$ que satisface hace que la cantidad demandada sea exactamente igual a la cantidad $q_{e}$ ofrecida a ese precio.
3. Cualquier punto sobre este precio de equilibrio presenta exceso de oferta.
4. Cualquier punto bajo este precio de equilibrio presenta escasez de demanda.

> ***Recordar:*** los económicos grafican al revés.
> Ejemplo:
> $$\begin{cases} q_{d}=30-20p \\ q_{s}=10+10p\end{cases}$$
> Punto de equilibrio:
> $$p_{e}=\frac{2}{3}=0.6667,\ q_{e}=\frac{50}{3}=16.6667$$

## Desplazamiento de la demanda y la oferta (Simultáneamente)
En estos casos calculas el área de triángulos:
- El triangulo de la curva de demanda entre $p_{e}$ y $p_{max}$es el *Excedente del consumidor* también definido como la diferencia que se genera entre el precio máximo que estaría dispuesto a pagar por cada unidad el consumidor y el precio que termina pagando (el ahorro)
- El triangulo de la curva de oferta entre $0$ y $p_{e}$ se le denomina *Excedente del productor* lo cual se define como la diferencia entre el precio que recibe el productor y el precio al que estaba dispuesto a vender cada unidad de producción (es la ganancia extra).
![enter image description here](https://lh3.googleusercontent.com/l0wq-YqkuIZH1jAzTkmXMIweyAKeHGT_3zspXF2yJcvh012ciwK7VBcBHslJrj3fIPLei6KbQus)
> ***Ejemplo:*** Dado las siguientes curvas determine
> $$\begin{cases} Q_{s}=p \\ Q_{d}=24-p\end{cases}$$
> 1. El punto de equilibrio del mercado $(p_{e},q_{e})$: *$(12, 12)$*
> 2. El excedente del consumidor: *72*
> 3. El excedente del productor: *72*
> 4. El excedente total: *144*
---

# Precios máximo y mínimos
![Grafico](https://lh3.googleusercontent.com/Klw0bP5BYUL0cJRJxTT3I41Ma_V651e5r1HdsgaSVVwahvZp10QPvaH4g0Z6Q90e_WbYDC2kAbo)
### Fijación de precios máximos
1. Sobre el equilibrio: Los precios tienden al $p_{e}$ dado el exceso de oferta.
![enter image description here](https://lh3.googleusercontent.com/QjqhJtlENBWEQiQt6WWZYlN0lDrUoNOWSA2toz9x_1GFkSOhTYYiZsLE5r28TqBaw_iv4b6gS3k)
2. Bajo el  equilibrio: Los precios no pueden ir al $p_{e}$ por lo que aparece el "mercado negro". Las ganancias extras se dan por $(p_{mn}-p_{max})\times q_{s}$
![enter image description here](https://lh3.googleusercontent.com/7-1Sc24bFbjA3TOuekdA2aKY0ZFSGc8vj0thUWJ9jV84e_C6PVPsr13eqB8YXn-dbScieGSTEjE)
### Fijación de precios mínimos
1. Sobre el equilibrio: En este caso el estado absorbe el exceso de oferta a razón de $p_{min}\times (q_{s}-q_{d})$
2. Bajo el equilibrio: La medida no tiene efecto.
# Elasticidad de la demanda
Mide la variación porcentual de la cantidad demandada frente a la variación porcentual de otra variable como por ejemplo le precio de bien, el ingreso del bien, el precio de un sustituto o el precio de un complementario.
Elasticidad es sinónimo de sensibilidad, por lo tanto cuando se dice que una demanda es elástica al precio significa que es sensible a las variaciones de precio.
- Elasticidad precio de la demanda:
$$E_{s}=\frac{\%Variación\ de\ la\ catidad\ demandada}{\%Variación\ del\ precio}$$
$$E_{s}=\frac{\%\Delta q}{\%\Delta p}$$ 
Casos:
- $E_{p}=1\ \Rightarrow\ \%\Delta p = \%\Delta q$, demanda unitaria
- $0<E_{p}<1\ \Rightarrow \ \%\Delta p > \%\Delta q$, demanda inelástica (bienes básicos).
Conviene subir el precio.
- $1<E_{p}<\infty \Rightarrow \ \%\Delta p < \%\Delta q$,  demanda elástica (bienes de lujo, prescindibles o suntuarios).
Conviene bajar los precios
### Casos especiales o extremos
- Demanda totalmente inelástica ($E_{p}=0$)
$$E_{p}=\frac{0}{x}$$
Ejemplos: Medicamentos vitales, drogas, adicciones.
- Demanda totalmente elástica ($E_{p}=\infty$)
## Elasticidad Arco
$$E_{arco}=(\frac{\Delta q}{\Delta p})\times (\frac{p_{1}+p_{2}}{q_{1}+q_{2}})$$
## Elasticidad Punto
Se calcula el coeficiente de elasticidad en un punto especifico de la curva de demanda.
$$E_{arco}=(\frac{\Delta q}{\Delta p})\times (\frac{p_{c}}{q_{c}})$$
### La elasticidad y el ingreso total
Tomando como referencia el punto en donde $p=q$ tendremos que:
$$E_{p}=(\frac{\Delta q}{\Delta p})\times (\frac{p}{q})$$
$$\lim_{q\to0}(\frac{p}{q})=\infty$$
$$\lim_{p\to0}(\frac{p}{q})=0$$
> Ejercicio: 
> $$q_{d}=1000-4p$$
> Suponga que la demanda por transbordo en el canal de tenglo está descrito por la ecuación, actualmente la empresa dueña del transbordador está cobrando $200 por pasaje.
> a) ¿Está maximizando sus ingresos totales?
> b) Si su respuesta es no, ¿debería subir o bajar el precio?
> c) ¿Cuánto debería cobrar?

## Elasticidad Demanda-Ingreso

$$E_{s}=\frac{\%Variación\ de\ la\ catidad\ demandada}{\%Variación\ del\ ingreso}$$
$$E_{s}=\frac{\%\Delta q}{\%\Delta y}$$ 

En esta elasticidad importa el signo dado que $E_{s}>0$ es bien normal, de lo contrario es bien inferior.
### Elasticidad Arco

$$E_{arco}=(\frac{\Delta q}{\Delta y})\times (\frac{y_{1}+y_{2}}{q_{1}+q_{2}})$$

### Elasticidad punto

$$E_{arco}=(\frac{\Delta q}{\Delta y})\times (\frac{y}{q})$$

## Elasticidad cruzada de la demanda

$$E_{xy}=\frac{\%Variación\ de\ la\ catidad\ demandada\ de\ x}{\%Variación\ de\ la\ catidad\ demandada\ de\ y}$$
$$E_{xy}=\frac{\%\Delta q_x}{\%\Delta p_y}$$
# Maximización de Beneficios en una Competencia Perfecta
> Recordar:
> - Muchos compradores
> - Productos homogeneos
> - No hay barrera de entrada o de salida
## Equilibrio de la empresa
- Al precio de mercado puede vender y/o producir todo lo que debe.
- La única variable que determina la empresa es el volumen de producción y/o ventas $q$.
- Donde $p=CM_g$ (costo marginal) la empresa máximiza beneficios o minimiza las pérdidas.
- Pero no le sirve cualquier $q$.
- Le servirá el $q$ que será el volumen de producción que máximiza sus beneficios o minimiza sus pérdidas.
- Ese $q$ determina mediante la siguiente relación: $$CM_g=\frac{\Delta Costo\ Total}{\Delta q}$$
---
| p | q | Ingreso Total ($pq$) | Costo Total | Beneficios |
|--|--|--|--|--|
| $$100$ | 1 | $$100$ | 90 | 10 |
| $$100$ | 2 | $$200$ | 195 | 5 |
| $$100$ | 3 | $$300$ | 310 | -10 |
$$CM_g=\frac{\partial CT}{\partial q}$$
$$p=\frac{\partial IT}{\partial q}, IT=pq$$
$$\Rightarrow \frac{\partial \Pi}{\partial q}=p-CM_g$$
En una competencia perfecta:
$$CM_g=CT´$$
$$IM_g=IT'=p$$

---
| p | q | Ingreso Total ($pq$) | Ingreso Marginal |
|--|--|--|--|--|
| $$100$ | 1 | $$100$ | $\frac{100}{1}=100$ |
| $$100$ | 2 | $$200$ | $\frac{200}{2}=100$ |
| $$100$ | 3 | $$300$ | $\frac{300}{3}=100$ |
---
> ***Ejercicio:*** 
> $$\begin{cases}q_d=250-2p \\ q_s=150+3p \\ CT=q^2+2q+9\end{cases}$$
> 1. Determinar el volumen de producción que maximiza los beneficios de la empresa.
> 2. Determinar la utilidad $\Pi$ con $q=10$ y $q=8$.
> ---
> 1. 
> $$q_d=q_s\Rightarrow 20=p_e$$
> $$p=CM_g\Rightarrow 20=2q+2\Rightarrow q^*=9$$
> $$IT=\Pi=20\times9-(9^2+2\times9+9)=72$$
> 2. 
> $$\Pi(10)=\Pi(8)=71$$
---
| $p$ | $q$| $IT=20q$ | $CT=q^2+2q+9$ | $CM_g=\frac{\Delta CT}{\Delta q}$ | $CTM_e=\frac{CT}{q}$ | $\Pi=IT-CT$ | 
|--|--|--|--|--|--|--|
| $20$ | 1 | $20$ | 12 | 4 | 12 | 8 |
| $20$ | 2 | $40$ | 17 | 6 | 8,5 | 23 |
| $20$ | 3 | $60$ | 24 | 8 | 8 | 36 |
| $20$ | 4 | $80$ | 33 | 10 | 8,25 | 47 |
| $20$ | 5 | $100$ | 44 | 12 | 8,8 | 56 |
| $20$ | 6 | $120$ | 57 | 14 | 9,5 | 63 |
| $20$ | 7 | $140$ | 72 | 16 | 10,285 | 68 |
| $20$ | 8 | $160$ | 89 | 18 | 11,125 | 71 |
| $20$ | 9 | $180$ | 108 | 20 | 12 | 72 |
| $20$ | 10 | $200$ | 129 | 22 | 12,9 | 71 |
| $20$ | 11 | $220$ | 152 | 24 | 13,818 | 68 |
| $20$ | 12 | $240$ | 177 | 26 | 14,75 | 63 |
| $20$ | 13 | $260$ | 204 | 28 | 15,692 | 56 |
| $20$ | 14 | $280$ | 233 | 30 | 16,642 | 47 |
| $20$ | 15 | $300$ | 264 | 32 | 17,6 | 36 |
---
## Beneficios y Perdidas
$$\Pi=(p-CTM_e)\times q^*$$

> $$\begin{cases}q_s=100+4p\\ q_d=370-5p\\ CT=2q^2+2q+7\end{cases}$$
> Si $q_s=q_d \Rightarrow p_e=30 \land q_e=220$
> $CM_g=\frac{dCT}{dq}=4q+2$
> Si $p=CM_g \Rightarrow q^*=7$
> $\Pi = 30*7-(2\times7^2+2\times7+7)=91$
> $\Pi_{u}=91\div 7=13$

- Punto de nivelación:
$IT=CT \Rightarrow p=CTM_e$
Punto en donde los costos son iguales a los costos, es decir, el productor no gana ni pierde nada.
- Punto de cierre:
$p\geq CVM_e$
Se sigue produciendo.
- Maximo ingresos:
$p=CM_g\Rightarrow q^*$

> ***Ejercicio:***
> La empresa de cartones corrugados produce cajas de cartón duro que son vendidas en paquetes de 1000 cajas. El mercado es competitivo con paquetes que se venden a $$100$. La curva de costos es $CT=3M+1mq^2$
> 1. Calcular cantidad que max. beneficios.
> 2. ¿Está la empresa obteniendo beneficios?
> 3. Analise la situación de la empresa. ¿Debe seguir operando o cerrar en el corto plazo?
> ---
> 1)
> $$p=\$100$$
> $$max\ \Pi\Rightarrow p=CM_g$$
> $$100=\frac{\partial CT}{\partial q}=2mq$$
> $$\Rightarrow q^*=\$50k$$
> 2) 
> $$\Pi=5M-(3M+1m\times (50k)^2)=-500k$$
> 3) 
> -Si cierra:
> $$\Pi=IT-CT$$
> $$\Pi=0-(3M+0^2)=-3M$$
> Pierde más dinero cerrando.
> 4) 
> $$p\geq CVM_e\Rightarrow se\ sigue\ operando$$
> $$100\geq \frac{CV}{q}$$
> $$100\geq \frac{1mq^{2\not}}{q\not}=q^*=50k$$
> $$100\geq 50$$

# Impuestos y Subsidios
## Aplicación de Impuestos
$T=P_d-P_s$ A cierto $q_T<q_e$
## Aplicación de Subsidio
$S=P_s-P_d$ A cierto $q_S>q_e$

---
$$\begin{cases}Q_d=100-P_d \\ Q_s=40+2P_s\\T=6\end{cases}$$
Cantidad demandada sin impuestos:
$$Q_d=Q_s\Rightarrow p_e=20,q_e=80$$
Con impuestos:
$$\Rightarrow P_d-P_s=6$$
$$P_d=6+P_s$$
$$Q_d=Q_s\Rightarrow P_s=18, P_d=24\Rightarrow Q_d=Q_s=76$$
Recaudación fiscal:
$$RF=T\times Q_T=6\times 76=456$$
Con $T=6$:
$$P_s=17.\overline{3},P_d=25.\overline{3},Q_s=Q_d=74.\overline6,$$
## Teoría de la Producción de Corto Plazo
Sea $Q$ el volumen de producción, $k$ el factor productivo y $L$ el factor productivo de trabajo.
$$Q=f(k,L)$$
Acorto plazo el factor productivo se considera fijo y se denota $\overline{k}$.
Definimos a continuación el Producto Medio como sigue:
$$PM_e=\frac{Q}{L}$$
Y se deriva de esta definición el Producto Marginal como:
$$PM_g=\frac{\partial Q}{\partial L}$$

---
|$$L$$ Cantidad de trabajo| $$Q$$ Producción total |  $$PM_e$$ Producto medio | $$PM_g$$ Producto marginal |
|--|--|--|--|
| 0 | 0 | --- | --- |
| 1 | 10 | 10 | 10 |
| 2 | 30 | 15 | 20 |
| 3 | 60 | 20 | 30 |
| 4 | 80 | 20 | 20 |
| 5 | 95 | 19 | 15 |
| 6 | 108 | 18 | 13 |
| 7 | 112 | 16 | 4 |
| 8 | 112 | 14 | 0 |
| 9 | 108 | 12 | -4 |
| 10 | 100 | 10 | -8 |
---
> Cuando $PM_g$ tenemos el maximo de $PT$
> Cuando $PM_g>PM_e$ entonces $PM_e$ esta creciendo
> Cuando $PM_g<PM_e$ el $PM_e$ esta decreciendo
> Cuando $PM_g=PM_e$ el $PM_e$ es maximo

## Etapas de la Producción
1. Va desde el origen hasta el $PM_e$ maximo
2. Va desde el punto del producto medio maximo hasta donde el $PM_g = 0$
3. Donde $PM_g<0$

# Macroeconomía
La macroeconomía centra su interés en las grandes variables e indicadores de la economía.
Le interesa el comportamiento del conjunto de los mercados.
El mercado específico de interés es el mercado del trabajo.

## Visión Panorámica de la Macroeconomía
| Área-Tema | Objetivo | Indicador |
|--|--|--|
| Nivel de Producción | Aumentarlo | Producto Interno Bruto (PIB) |
| Nivel de Precios | Estabilizarlo | Indice de Precios del Consumidor |
| Nivel de Empleos | Aumentarlo | Taza de Desempleo |
| Sector Externo | Estabilizarlo | Balanza Comercial, Balanza de Pago |

### Nivel de Producción
El crecimiento es una condición necesaria pero no suficiente para el desarrollo economico.
El *PIB* es el valor de todos bienes y servicios finales, producidos por un país en un determinado período de tiempo generalmente un año.
$$\sum_{i=1}^np_iq_i$$

> Indice de mensual de actividad economica (imacec)
> PIB percapita

---

# Cálculo de IPC
| Producto | $q$ | $p_{2016}$ | Gasto | $p_{2017}$ | Gasto | $p_{2018}$ | Gasto |
|--|--|--|--|--|--|--|--|
| A | 5 | 10 | 50 | 11 | 55 | 12 | 60 |
| B | 60 | 0,5 | 30 | 0,6 | 36 | 0,75 | 45 |
| C | 4 | 5 | 20 | 4 | 16 | 5 | 20 |
| Costo Canasta Base | --- | --- | 100 | --- | 107 | --- | 125 |

$$Tasa\ Inflación_{2017\ sobre\ 2016}=\frac{IPC_{2017}-IPC_{2016}}{IPC_{2016}}\times 100=7\%$$
$$Tasa\ Inflación_{2018\ sobre\ 2017}=\frac{IPC_{2018}-IPC_{2017}}{IPC_{2017}}\times 100=16,8\%$$
$$Tasa\ Inflación_{2018\ sobre\ 2016}=\frac{IPC_{2018}-IPC_{2016}}{IPC_{2016}}\times 100=25\%$$

## Inflación
Por demanda ocurre que el aumento de demanda de un bien aumenta los costos generando mayor inflación.
Por costo ocurre que el aumento del mismo se traspasa ese costo al precio de los productos generando tambien mas inflación.
## PIB Nominal y Real
PIB Nominal: La producción de bienes y servicios finales se valoriza a los precios de cada año (precios corrientes).
PIB Real: La produccón de bienes y servicios finales se valoriza a precios de un año base (precio constante).
$$PIB=\sum_{i=1}^np_iq_i$$
> ***Ejemplo:***
> 
| Año | $p$ | $q$ | $p$ | $q$ | $PIB_{nominal}$ |  $PIB_{real}$ |
|--|--|--|--|--|--|--|
| 2016 | 100 | 20 | 100 | 30 | 5000 |  5000  |
| 2017 | 90 | 20 | 105 | 20 | 3900 |  4000  |
| 2018 | 105 | 20 | 110 | 40 | 7550 |  7000  |

$$Tasa\ de\ Crecimiento=\frac{PIB_{real\ 2017}-PIB_{real\ 2016}}{PIB_{real\ 2016}}\times 100=-20\%$$

Deflactor PIB: Es un índice de precios que permite obtener el PIB real, a partir del PIB nominal.
$$Deflactor=\frac{PIB_{nominal}}{PIB_{real}}\times 100$$

# Características del PIB
- No mide bienestar
- No considera el impacto de la contaminación por lo que sobrevalora la producción
- No considera el mejoramiento de la calidad de los bienes
- No considera los trabajos realizados por uno mismo (Ej: cortad el césped, pintar la casa, etc)
- No considera el mercado informal
# Instrumentos de la Política Macroeconómica
1. Política Fiscal: La maneja el ministerio de hacienda
	- Gasto público
		- Compras del estado
		- Sueldos funcionarios públicos
		- Inversiones varias
	- Impuestos
		- Tiene por finalidad aumentar los ingresos del fisco o disminuir el consumo de algunos bienes y servicios
		- Impuestos Directos: Afectan el ingreso
		- Impuestos Indirectos: Afectan al consumo
	- Transferencias: Estipendios en dinero
2. Política Monetaria: Manejada por el Banco Central
3. Políticas de Rentas: Se relaciona con la fijación de precios máximos y minimos.

## Formas de Medición del PIB
1. Método del Gasto: Midiendo el gasto de cada agente económico
$$PIB=Consumo_{familias} + Inversión_{empresas} + Gasto\ Público_{estado} + (Exportaciones - Importaciones)_{sector\ externo}$$
2. Método del Valor Agregado: Consiste en sumar el valor que agregan a la producción los distintos sectores de economía.
3. Método de Ingresos o Rentas: Se suman los pagos que reciben los factores productivos.
	- Tierra $\Rightarrow$ Renta
	- Trabajo $\Rightarrow$ Salarios
	- Capital $\Rightarrow$ Interés
	- Capacidad Empresarial $\Rightarrow$ Utilidades o Beneficios

# Inflación Subyacente
Es la inflación que se atribuye al alza de los precios de los *commodities*.
