# Inferencia Estadística
Podemos resumir la inferencia estadística en dos partes:
- El estudio que busca generar estimadores a valores poblacionales en base a muestras.
- El estudio de prueba de hipótesis relacionadas a estudios estadísticos.
# Teorema del Límite Central
Sea $N(\mu , \sigma ^2)$ la función de densidad de la distribución normal definida como:
$$f_{\mu, \sigma^2}(x)=\frac{1}{\sqrt{2\pi \sigma^2}}e^{-\frac{(x-\mu)^2}{2\sigma^2}}$$
con media $\mu$ y varianza $\sigma^2$.

Sea $F_{\overline{\theta}}(x_i)$ una función densidad cualquiera con v.a. $x_i$, parámetros $\overline{\theta}$ y una muestra aleatoria de tamaño $n$ se tiene por el teorema del límite central:
$$\Rightarrow\overline{x}\sim N\left(\mu, \frac{\sigma^2}{n}\right)$$
# Insesgamiento
## Estimadores
Se define un estimador para la función de densidad de una distribución de la forma que sigue:
$$f_{\theta}=\sum_{i=0}^{n}\alpha_i\theta_i\ /\  \forall i=1,2,3,...,n$$
donde $\theta_i$ es v.a. y $\alpha_i$ es una constante.
Decimos que el estimador es insesgado si al aplicar esperanza $E(f_\theta)$ el resultado es igual a la media poblacional de la distribución a la que se encuentre estimando, asumiendo que $\theta_i$ pertenece a la distribución.
## Múltiples Estimadores
Cuando se tienen más de un estimador se prefiere el que tenga menor varianza, es decir, aplicamos $V(f_\theta)$.
> En ocasiones esto no es suficiente y requeriremos de un método que nos permita establecer cotas a las varianzas de los estimadores.
# Cota Cramer-Rao
La cota de Cramer-Rao es una cota inferior a la varianza de un estimador cualquiera cumpliendo que sea insesgado y se define como sigue:
$$V(\overline{\theta})\geq \frac{1}{nE\left(\left[\frac{\partial}{\partial\theta}ln(f_{\theta}(x))\right]^2\right)}$$
donde $f_{\theta}(x)$ es la función de densidad del estimador.
# Métodos de Estimación Puntual
## Máxima Verosimilitud
Sea $X\sim f_{\theta}(x)$ una función de densidad con $k$ parámetros $\theta_i$ para $i=1,2,3,...,k$ con una muestra aleatoria de tamaño $n$ tal que $X = x_1,x_2,...,x_n$ definimos la funcón de máxima verosimilitud como:
$$L(\theta) = \prod_{i=1}^nf_\theta(x_i)$$
En orden de usar la función es necesario aplicar métodos de optimización de cálculo diferencial para cada parametro que se desee estimar.
## Método de los Momentos
Este método resuelve, mediante un sistema de ecuaciones, los $k$ parámetros buscados igualando los $k$ primeros momentos poblacionales con los $k$ primeros momentos muestrales, esto es:
$$\begin{cases} E(x_i)=M_j \\ E(x_i)=\begin{cases} \sum_{i=1}^{k}x_i p(x_i),\ caso\ discreto \\ \int_{-\infin}^\infin x f(x)\partial x,\ caso\ continuo\end{cases}\end{cases}$$
# Técnicas de Muestreo y Tamaño Muestral
## Problemas de Muestreo
A la hora de querer realizar un muestreo existen factores a tener en consideración:
- Tamaño de la muestra:
	- Si la muestra es demasiado pequeña caemos en el problema de que los estimadores pueden no ser representativos de la realidad de la población.
	- Si la muestra es muy grande se cae en el absurdo de para que tomar muestra en primer lugar.
- Sesgo al tomar la muestra:
	- Los valores estadísticos que se calculen serán erroneos si se cae en un sesgo al  muestrear elementos de una población.
	- El contexto en el que se mide o muestrea una poblacón puede hacer que los valores estadísticos no sean representativos de la población.
## Tipos de Muestreo
### Muestreo Aleatorio Simple
Se elige un tamaño de muestra $n$ menor al tamaño de la población y se toman elementos de la población de manera aleatoria.
La cantidad de combinaciones posibles de muestras de tamaño $n$ es una población de tamaño $N$ esta dada por:
$$C_{n}^{N}={N\choose n}=\frac{N!}{n!(N-n)!}$$
> Los criterio sobre la elección del tamaño de la muestra se ve más adelante.
### Muestreo Aleatorio Estratificado
Se divide a la población en subgrupos a los cuales se le saca un tamaño de muestra "representativo" y se muestrea cada grupo para terminar con una muestra total de la población.
### Muestreo Sistematico
Se basa en tomar muetras en cantidades o intervalos de tiempo $k$ hasta tener la muestra deseada $n$ de la poblacón de tamaño $N$.
$$k=\left\lfloor\frac{N}{n}\right\rfloor$$
### Muestreo por Conglomerado
Se divide la población en $m$ partes iguales y se seleccionan $n$ de esos grupos, el total de esos $n$ grupos es nuestra muestra.
# Estimación de Parámetros Poblacionales
***Lista de parámetros:***
- Hipergeometrica: 0
- Binomial: 1
- Geometrica: 1
- Poisson: 1
- Uniforme: 0
- Exponencial: 1
- Normal: 2
## Estimación por Intervalo
En la esta estimación se busca llegar a un intervalo que nos permita saber entre que valores se puede contener nuestro parámetro poblacional.
Los requisitos para emprender estos estudios son:
1. Una muestra de tamaño $n$
2. Un nivel de confianza definido $1-\alpha$ talque $0.9\leq 1-\alpha \leq 1$
3. La distribución de probabilidad asociada a los estimadores
> Es importante que a partir de ahora se entenderá como muestra grande a cualquier muestra con $n\geq 30$
## Cálculo de Intervalos de Confianza
### Media Poblacional $\mu$
$$\begin{cases} \mu \in (\overline{x}\pm Z_{1-\frac{\alpha}{2}}\frac{\sigma}{\sqrt n}),\ n\geq 30 \\ \mu \in (\overline{x}\pm t_{1-\frac{\alpha}{2};n-1}\frac{s}{\sqrt n}),\ n<30 \end{cases}$$
donde $Z_{1-\frac{\alpha}{2}}$ es el estadístico para la tabla ***Normal*** y $t_{1-\frac{\alpha}{2};n-1}$ es para el estadístico para la tabla ***T-Student***.
> En el caso de que se desconozca la varianza poblacional se puede reemplazar por la muestral.
> El estadístico $t_{1-\frac{\alpha}{2};n-1}$ solo se usa cuando se cumple que la varianza poblacional es desconocida y $n$ pequeño.
### Varianza Poblacional $\sigma^2$
$$\left(\frac{(n-1)s^2}{X_{1-\frac{\alpha}{2};n-1}^2}\le \sigma^2\le \frac{(n-1)s^2}{X_{\frac{\alpha}{2};n-1}^2}\right)$$
donde $X_{1-\frac{\alpha}{2};n-1}^2$ y $X_{\frac{\alpha}{2};n-1}^2$ son los estadísticos para la tabla ***Xi Quadrat***.
> En caso de querer estimar la desviación estándar basta con calcular las raíces del los extremos de los intervalos.
### Proporción Poblacional
$$p\in \left(\overline{p}\pm Z_{1-\frac{\alpha}{2}}\sqrt{\frac{\overline{pq}}{n}}\right)$$
con $\overline{p}=\frac{x}{n}$ y $\overline{q}=1-\overline{p}$.
> Para este intervalo usamos el estadístico de la ***Normal*** siempre que $P$ no sea cercano a $0$ ó $1$ y siempre que $n$ sea ***grande***.
### Diferencia de Medias
$$\begin{cases} (\mu_1-\mu_2)\in \left((\overline{x}_1-\overline{x}_2)\pm Z_{1-\frac{\alpha}{2}}\sqrt{\frac{\sigma_1^2}{n_1}+\frac{\sigma_2^2}{n_2}}\right),\ n\ge 30 \\ (\mu_1-\mu_2)\in \left((\overline{x}_1-\overline{x}_2)\pm t_{1-\frac{\alpha}{2};n_1+n_2-2} \sqrt{\frac{(n_1-1)s_1^2+(n_2-1)s_2^2}{n_1+n_2-2}\left(\frac{\sigma_1^2}{n_1}+\frac{\sigma_2^2}{n_2}\right)}\right),\ n<30\end{cases}$$
> Misma observación que para la media de una sola muestra.
### Cuociente de Variazas
$$\frac{1}{F_{1-\frac{\alpha}{2};(n_2-1, n_1-1)}}\times \frac{s_2^2}{s_1^2}\le\frac{\sigma_2^2}{\sigma_2^1}\le F_{1-\frac{\alpha}{2};(n_1-1, n_2-1)}\times \frac{s_2^2}{s_1^2}$$
donde $F_{1-\frac{\alpha}{2};(n_1-1, n_2-1)}$ y $F_{1-\frac{\alpha}{2};(n_2-1, n_1-1)}$ corresponden al estadístico de la tabla ***F-Fisher***
### Diferencia de Proporciones Poblacionales
$$(p_1-p_2)\in \left((\overline{p}_1-\overline{p}_2)\pm Z_{1-\frac{\alpha}{2}}\sqrt{\frac{\overline{p}_1\overline{q}_1}{n_1}+\frac{\overline{p}_2\overline{q}_2}{n_2}}\right)$$
> Mismas consideraciones que con la Proporcion Poblacional de una sola muestra.
### Tamaño de la Muestra
Para la Distribución Normal:
$$n\ge\left(Z_{1-\frac{\alpha}{2}}\frac{\sigma}{\beta}\right)^2$$
Para la Proporción  Poblacional:
$$n\ge\left(\frac{Z_{1-\frac{\alpha}{2}}\sqrt{\overline{pq}}}{\beta}\right)^2$$
donde $\beta$ es el error estándar medio.
# Prueba de Hipótesis
Las pruebas de hipótesis son un procedimiento estadístico que permite determinar la veracidad de un valor particular que se propone para un parámetro poblacional de interés.
1. Hipótesis Nula $H_0$: corresponde a la afirmación de igualdad que se busca probar.
2. Hipótesis Alternativa $H_1$: corresponde a la afirmación que se aprueba en caso de rechazar $H_0$.
3. Estadístico del *Test*: corresponde al estadístico específico a utilizar para realizar la prueba.
4. Región Crítica o Zona de Rechazo: corresponde a la comparación del resultado del estadístico con el nivel de confianza.
5. Conclusión: corresponde a la interpretación del resultado con respecto a las aseveraciones realizadas.
---
***Ejemplo:***
> 1. $H_0$: $\mu=\mu_0$
> 2. $H_1$: $\mu<\mu_0 \vee \mu =\not \mu_0 \vee \mu >\mu_0$
> 3. Prueba de Test: $\overline{x}\sim N\left(\mu,\frac{\sigma^2}{n}\right)$ $$Z_{v-p}=\frac{\overline{x}-\mu_0}{\sigma}\sqrt{n}$$ $$\begin{cases}v-p<\alpha,\ se\ rechaza\ H_0 \\v-p\ge \alpha,\ no\ se\ rechaza\ H_0\end{cases}$$
> 4. Se concluye en base a la hipótesis que se haya probado.
---
> ****Su usan los estadisticos bajo las mismas condiciones mencionadas en temas anteriores.***
## Estadísticos
- Para la media:
$$\begin{cases}t_{v-p;n-1}=\frac{\overline{x}-\mu_0}{s}\sqrt{n},\ Varianza\ desconocida\ y\ n<30\\Z_{v-p}=\frac{\overline{x}-\mu_0}{\sigma}\sqrt{n},\ Cualquier\ otro\ caso\end{cases}$$
- Para la varianza poblacional:
$$X_{v-p;n-1}^2=\frac{(n-1)s^2}{\sigma_0^2}$$
- Para la proporción poblacional:
$$Z_{v-p}=\frac{\overline{p}-p_0}{\sqrt{p_{0}\ q_{0}}}\sqrt{n}$$
- Para la diferencia de medias:
$$\begin{cases}t_{v-p;n-1}=\frac{(\overline{x}_1-\overline{x}_2)-D_0}{\sqrt{\frac{(n_1-1)s_1^2+(n_2-1)s_2^2}{n_1+n_2-2}\left(\frac{\sigma_1^2}{n_1}+\frac{\sigma_2^2}{n_2}\right)}},\ Varianza\ desconocida\ y\ n<30\\Z_{v-p}=\frac{(\overline{x}_1-\overline{x}_2)-D_0}{\sqrt{\frac{\sigma_1^2}{n_1}+\frac{\sigma_2^2}{n_2}}},\ Cualquier\ otro\ caso\end{cases}$$
donde $D_0=(\mu_1-\mu_2)$.
> Cuando se deba caer en usar el estadístico $t_{v-p;n-1}$ se debe probar la igualdad de varianzas, de rechazarse esta no se continua con el estudio y se concluye.
- Para el cuociente de varianzas:
$$F_{v-p;n_1-1;n_2-1}=\frac{s_1^2}{s_2^2}$$
- Para la diferencia de proporciones poblacionales:
$$Z_{v-p}=\frac{(\overline{p}_1-\overline{p}_2)-D_0}{\sqrt{\overline{q}\left(\frac{n_1\overline{p}_1+n_2\overline{p}_2}{n_1+n_2}\right)\left(\frac{1}{n_1}+\frac{1}{n_2}\right)}}$$
donde $D_0=(p_1-p_2)$.
# Prueba de Bondad de Ajuste
Son pruebas no parametricas que buscan probar si una distribución observada corresponde a una distribución teórica.
## Pruebas Xi Quadrat
$$X^2=\sum^{k}_{i=1}\frac{(o_i-e_i)^2}{e_i}$$
donde $o_i$ es la frecuencia observada del i-ésimo valor de la variable de estudio y $e_i$ es la frecuencia esperada del i-ésimo valor de la variable en estudio.

La distribución muestral del esstadistica $X^2$ es aproximadamente Xi Quadrat con $(k-m-1)$ grados de libertad y un nivel de significancia $\alpha$:
$$X^2\sim X^2_{1-\alpha;k-m-1}$$
donde $k$ es el número de valores que toma la variable de estudio y $m$ el número de parámetros considerados en la distribución teórica.

Se rechaza la hipótesis nula si se cumple que:
$$X^2>X^2_{1-\alpha;k-m-1}$$
***Observaciones***
1. Cada parámetro es considerado en una distribución teórica es reemplazado por un estimador de máxima verosimilitud
> Ejemplo:
> $$M\Rightarrow\overline{x}$$
> $$\sigma^2\Rightarrow s^2$$
> $$p\Rightarrow E(x)=\frac{1}{p}=\overline{x}=\frac{n}{\sum x_i}$$
2. Para efecto de convergencia la prueba Xi Quadrat se satisface sólo si $e_i\geq5$ y $n\geq5$ con $n$ siendo el número de observaciones.
> Cuando no se cumple se debe agrupar los menores valores hasta cumplir con estos requisitos.
---

Clase que falté

---

# Regresión Lineal
Es la aproximación de una poblacion de puntos a una recta de la forma $$y=a+bx$$
y su indice de correlación que mide que tan acertada es la recta se calcula $$r=\frac{n\sum y_ix_i-\sum y_i\sum x_i}{\sqrt{[n\sum x_i^2-(\sum x_i)^2][n\sum y_i^2-(\sum y_i)^2]}}$$
### Regreción Lineal Simple
Representamos la recta con los siguientes parámetros:$$\overline{y}=\hat{\beta_0}+\hat{\beta}_1\overline{x}$$
$$\overline{x}=\frac{\sum x_i}{n}, \overline{y}=\frac{\sum y_i}{n}$$
$$\beta_1=\frac{n\sum y_ix_i-\sum y_i\sum x_i}{n\sum x_i^2-(\sum x_i)^2}$$
$$\Rightarrow \hat{\beta_0} = \overline{y}-\hat{\beta}_1\overline{x}$$
Markdown 12270 bytes 1508 words 164 lines Ln 1, Col 0HTML 13304 characters 1481 words 154 paragraphs
