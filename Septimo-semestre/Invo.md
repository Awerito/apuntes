# Investigación Operativa
Es el estudio de cómo formular **modelos matemáticos** para problemas complejos de administración e ingeniería, y cómo analizarlos analizarlos y resolverlos para tener una visión de las posibles soluciones.
## Modelamiento
### Definición del Problema
Se comienza por definir la frontera del sistema que se desea modelar identificando sus distintas componentes que lo compone así como tambien el objetivo del mismo.
Es importante definir desde un inicio las variables sobre las que tenemos control en el sistema porque serán la variables que controlen el modelo matemático final.
### Construcción de un Modelo
Hay pasos a seguir para la construcción de un modelo que se deben seguir una vez definido el problema:
1. Se deben reconocer y definir de manera consistente las variables que participarán en nuestro modelo, estas pueden ser binarias, numéricas continuas o discretas, etc.
2. Plantear las restricciones que pueda tener nuestro sistema en base a su propio comportamiento o requerimientos personales delimitando así una frontera de posibilidades.
3. Resolución del problema considerando las especificaciones que se deseen cumplir por medio de algún método de resolución de modelos.
### Problema General de Optimización
Sean $x_1, x_2, x_3, ... ,x_n=\vec{x}$ variables de decisión donde:
$$\vec{x}\in S \subseteq \R^n$$
donde $S$ es el conjunto de soluciones factibles.
Definimos entonces nuestra función objetivo de manera que:
$$f:\R^n\longrightarrow \R$$
luego el problema a resolver es:
$$min \lor max(f(\vec{x})) $$
> Si $S=\R^n$ entonces el problema es irrestricto.

$$g_i:\R^n\longrightarrow\R$$
$$s.a.\ g_i(\vec{x})\leq 0, con\ i=1,2,...,m$$
### Problemas Lineales
Un problema lineal donde $f$ y $g_i$ son de la forma:
$$a^Tx+b$$
con $a^T\in \R^n$ y $b \in \R$.
### Problemas No Lineales
Cualquier función del modelo que tengo un termino variable no lineal.

---
|$x_1$| $x_2$ |
|--|--|
| 3 | 4 |
| 1 | -1 |
| -2 | 1 |
---
# Dualidad
Se combierte un problema de maximización es uno de minimización equivalente.
$$a_ix_i\geq b_i\rightarrow y_i\geq0$$
$$a_ix_i\leq b_i\rightarrow y_i\leq0$$
$$a_ix_i= b_i\rightarrow y_i\ Sin\ restricción$$
$$x_i\geq0\ \ \ b_iy_i\leq 0$$
$$x_i\leq0\ \ \ b_iy_i\geq 0$$
$$x_i=0\ \ \ b_iy_i=$$
